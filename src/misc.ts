export const genIframeDoc = (svg: string) => `<!doctype html>
<head>
  <meta charset="UTF-8"/>
  <title>such valid html, wow</title>
  <style>
  body {
    text-align: center;
  }
  svg {
    background-image:
      linear-gradient(45deg, #ccc 25%, transparent 25%),
      linear-gradient(-45deg, #ccc 25%, transparent 25%),
      linear-gradient(45deg, transparent 75%, #ccc 75%),
      linear-gradient(-45deg, transparent 75%, #ccc 75%);
    background-size: 20px 20px;
    background-position: 0 0, 0 10px, 10px -10px, -10px 0px;
    border: 1px dotted black;
  }
  </style>
<head>
<body>
  ${svg}
</body>`

export const initialSvg = `<svg xmlns="http://www.w3.org/2000/svg" width="400" height="400" version="1.1">
  <rect x="50" y="50" width="100" height="100" stroke="blue" stroke-width="20" fill="purple" />
  <circle cx="50" cy="50" r="50" fill="orange" />
  <polygon points="100,10 250,190 160,210" stroke="black" stroke-width="7" fill="pink" />
</svg>`
