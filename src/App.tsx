import * as React from 'react'
import * as style from './style'
import { initialSvg, genIframeDoc } from './misc'

interface AppState {
  svg: string
  iframeDoc: string
}

export default class App extends React.Component<{}, AppState> {
  state = {
    svg: initialSvg,
    iframeDoc: genIframeDoc(initialSvg),
  }

  render() {
    return (
      <div style={style.container}>
        <textarea
          style={style.textarea}
          value={this.state.svg}
          onChange={this.onChange.bind(this)}
        />
        <iframe style={style.iframe} srcDoc={this.state.iframeDoc} />
      </div>
    )
  }

  onChange(e) {
    const svg = e.target.value
    const iframeDoc = genIframeDoc(svg)
    this.setState({ svg, iframeDoc })
  }
}
