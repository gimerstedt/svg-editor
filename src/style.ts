export const textarea = {
  fontFamily: '"fira code", mono',
  fontSize: '14px',
  padding: '25px',
  width: '50%',
}

export const iframe = {
  width: '50%',
}

export const container = {
  display: 'flex',
  height: '100vh',
}
